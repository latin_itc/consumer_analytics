import argparse

from analyzer import VideoAnalizer, EntranceAnalizer


def main():
    ap = argparse.ArgumentParser()

    ap.add_argument("-i", "--input", type=str, help="path to optional input video file")
    ap.add_argument("-o", "--output", type=str, help="path to optional output video file")
    ap.add_argument("-c", "--detection_confidence", type=float, default=0.3,
                    help="minimum probability to filter weak detections")
    ap.add_argument("-s", "--skip_frames", type=int, default=5, help="# of skip frames between detections")
    ap.add_argument("-t", "--tracking_confidence", type=int, default=10,
                    help="minimum probability to filter weak tracking")
    ap.add_argument("-n", "--nms", type=float, default=0.4, help="threshold for non max suppression")
    ap.add_argument("-ei", "--entrance_input", type=str, help="path to optional input video file")
    ap.add_argument("-eo", "--entrance_output", type=str, help="path to optional output video file")

    if ap.parse_args().input is not None:
        VideoAnalizer(ap.parse_args()).analize()
    if ap.parse_args().entrance_input is not None:
        EntranceAnalizer(ap.parse_args()).analize()


if __name__ == '__main__':
    main()
