import datetime

import cv2
import dlib
import imutils
import numpy as np
from imutils.video import FPS
from scipy.spatial import distance as dist

from detection.yolo_detector import YOLOPersonDetector
from pos_connector import PointOfSale
from selector.selector import CustomerSelector
from settings import *
from tracking.centroidtracker import CentroidTracker, EntranceCentroidTracker
from tracking.trackable import Trackable


class VideoAnalizer:
    def __init__(self, args):
        self.input = args.input
        self.output = args.output
        self.confidence = args.detection_confidence
        self.skip_frames = args.skip_frames
        self.min_tracker_confidence = args.tracking_confidence
        self.nms_threshold = args.nms
        self.trackers = []
        self.trackable_objects = {}
        self.curent_frame_number = 0

    def analize(self):
        print("[INFO] opening video file...")
        vs = cv2.VideoCapture(self.input)
        fps = FPS().start()
        ct = CentroidTracker(max_disappeared=30, max_distance=60)
        self.detect_and_track(vs, fps, ct)
        fps.stop()
        print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
        vs.release()
        cv2.destroyAllWindows()

    def detect_and_track(self, vs, fps, ct):
        width, height, writer = None, None, None
        yolo = YOLOPersonDetector(YOLO_CONFIG, YOLO_WEIGHTS, YOLO_CLASSES, self.confidence, self.nms_threshold)

        while True:
            trackers_to_delete = []

            frame = vs.read()[1]
            # get frame number
            self.curent_frame_number = int(vs.get(cv2.CAP_PROP_POS_FRAMES))

            if frame is None:
                break

            frame = imutils.resize(frame, width=500)
            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            self.remove_old_images(self.curent_frame_number)
            self.verify_if_there_is_a_payment(frame)

            if width is None or height is None:
                (height, width) = frame.shape[:2]
                fourcc = cv2.VideoWriter_fourcc(*"MJPG")
                writer = cv2.VideoWriter(self.output, fourcc, 30, (width, height), True)

            if self.curent_frame_number % self.skip_frames == 0:
                status = "Detecting"
                boxes = yolo.predict(frame)

                trackers_to_delete = self.match_trackers_with_detections(boxes, rgb)
            else:
                status = "Tracking"

            rects = self.generate_squares_from_tracker(rgb, height, width, trackers_to_delete)
            objects = ct.update(rects)
            self.print_objects(frame, height, objects, status, width)

            writer.write(frame)

            cv2.imshow("Frame", frame)
            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                break

            fps.update()

        writer.release()
        self.save_trackable_info_to_file()

    def match_trackers_with_detections(self, detections, rgb, max_distance=50):

        if len(detections) == 0:
            return self.trackers

        trackers_to_delete = []

        detections_centroids = np.zeros((len(detections), 2), dtype="int")
        trackers_centroids = np.zeros((len(self.trackers), 2), dtype="int")

        for (i, positions) in enumerate(detections):
            c_x, c_y = self.get_centroid(*positions)
            detections_centroids[i] = (c_x, c_y)

        for (i, tracker) in enumerate(self.trackers):
            tracker.update(rgb)
            pos = tracker.get_position()
            c_x, c_y = self.get_centroid(*self.get_positions_x_y(pos))
            trackers_centroids[i] = (c_x, c_y)

        dist_obj_input = dist.cdist(trackers_centroids, detections_centroids)

        rows = dist_obj_input.min(axis=1).argsort()
        cols = dist_obj_input.argmin(axis=1)[rows]

        used_rows, used_cols = set(), set()

        for (row, col) in zip(rows, cols):
            if row in used_rows or col in used_cols:
                continue

            if dist_obj_input[row, col] > max_distance:
                continue

            self.trackers[row] = self.create_new_tracker(rgb, *detections[col])
            used_rows.add(row)
            used_cols.add(col)

        unused_rows = set(range(0, dist_obj_input.shape[0])).difference(used_rows)
        unused_cols = set(range(0, dist_obj_input.shape[1])).difference(used_cols)

        for row in unused_rows:
            tracker = self.trackers[row]
            confidence = tracker.update(rgb)
            if confidence < self.min_tracker_confidence:
                trackers_to_delete.append(tracker)

        for col in unused_cols:
            tracker = self.create_new_tracker(rgb, *detections[col])
            self.trackers.append(tracker)

        return trackers_to_delete

    def generate_squares_from_tracker(self, rgb, height, width, trackers_to_delete):
        rects = []

        for tracker in self.trackers.copy():
            tracker.update(rgb)
            pos = tracker.get_position()
            c_x, c_y = self.get_centroid(*self.get_positions_x_y(pos))

            if self.is_outside_tracking_area(c_x, c_y, height, width) or tracker in trackers_to_delete:
                self.trackers.remove(tracker)
            else:
                rects.append((self.get_positions_x_y(pos)))

        return rects

    def print_objects(self, frame, height, objects, status, width):
        for objectID, value in objects.items():
            centroid = value['centroid']

            to = self.trackable_objects.get(objectID, None)

            if to is None:
                to = Trackable(objectID, list(centroid))

            to.centroids.append(list(centroid))
            to.frames.append(self.curent_frame_number)
            self.trackable_objects[objectID] = to
            cv2.circle(frame, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

        text = "Status: {}".format(status)
        cv2.putText(frame, text, (10, height - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)

    def is_outside_tracking_area(self, c_x, c_y, height, width):
        return (c_x >= (width // 3) + 100 and c_y >= height - (height // 11)) or \
               (c_x >= width - (width // 3) and c_y <= (height // 4)) or \
               ((width // 3) - 60 <= c_x <= (width // 3) + 50 and c_y <= height // 4)

    def get_centroid(self, start_x, start_y, end_x, end_y):
        return int((start_x + end_x) / 2.0), int(end_y)

    @staticmethod
    def get_positions_x_y(pos):
        return int(pos.left()), int(pos.top()), int(pos.right()), int(pos.bottom())

    @staticmethod
    def create_new_tracker(rgb, start_x, start_y, end_x, end_y):
        rect = dlib.rectangle(start_x, start_y, end_x, end_y)
        tracker = dlib.correlation_tracker()
        tracker.start_track(rgb, rect)
        return tracker

    def save_trackable_info_to_file(self):
        with open(os.path.join(os.getcwd(), CENTROIDS_OUTPUT_FILENAME), 'w') as f:
            for track_id, trackable in self.trackable_objects.items():
                for centroid, frame in zip(trackable.centroids, trackable.frames):
                    sec = datetime.timedelta(seconds=int((frame / FRAMES_PER_SEC)))
                    session = datetime.datetime(1, 1, 1) + sec
                    session = (session.hour, session.minute, session.second)
                    f.write('{},{},{},{}\n'.format(track_id, centroid[0], centroid[1], session))

    def verify_if_there_is_a_payment(self, frame):
        pass

    def remove_old_images(self, image):
        pass


class EntranceAnalizer(VideoAnalizer):
    def __init__(self, args):
        VideoAnalizer.__init__(self, args)
        self.input = args.entrance_input
        self.output = args.entrance_output
        self.total_in = 0
        self.total_out = 0
        self.pos = PointOfSale()
        self.selector = CustomerSelector()
        self.payment_frame = self.pos.get_next_frame()

    def analize(self):
        print("[INFO] opening video file...")
        vs = cv2.VideoCapture(self.input)
        fps = FPS().start()
        ct = EntranceCentroidTracker(max_disappeared=30, max_distance=60)
        self.detect_and_track(vs, fps, ct)
        fps.stop()
        print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
        print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
        vs.release()
        cv2.destroyAllWindows()

    def is_outside_tracking_area(self, c_x, c_y, height, width):
        return c_x < width // 3 or c_y > 3 * (height // 8)

    def get_centroid(self, start_x, start_y, end_x, end_y):
        return int((start_x + end_x) / 2.0), int((start_y + end_y) / 2.0)

    def print_objects(self, frame, height, objects, status, width):
        y_when_x_is_zero = 4 * (height // 5)
        x_when_y_is_zero = 3 * (width // 4)
        cv2.line(frame, (0, y_when_x_is_zero), (x_when_y_is_zero, 0), (255, 0, 0), 2)

        for objectID, value in objects.items():
            centroid = value['centroid']
            box = value['rectangle']

            to = self.trackable_objects.get(objectID, None)

            if to is None:
                to = Trackable(objectID, list(centroid))
            else:
                to.centroids.append(centroid)

                if not to.counted_in and \
                        centroid[1] > self.get_y_position_of_entrance_rect(centroid[0], x_when_y_is_zero,
                                                                           y_when_x_is_zero) and \
                        self.has_been_above_the_line(to.centroids, x_when_y_is_zero, y_when_x_is_zero):
                    self.total_in += 1
                    to.counted_in = True

                    self.selector.get_customer_entering(frame, box, self.curent_frame_number)
                elif not to.counted_out and \
                        centroid[1] < self.get_y_position_of_entrance_rect(centroid[0], x_when_y_is_zero,
                                                                           y_when_x_is_zero) and \
                        self.has_been_below_the_line(to.centroids, x_when_y_is_zero, y_when_x_is_zero):
                    self.total_out += 1
                    to.counted_out = True

                    self.selector.get_customer_leaving(frame, box, self.curent_frame_number)

            self.trackable_objects[objectID] = to

        info = [
            ("In", self.total_in),
            ("Out", self.total_out),
            ("Status", status),
        ]

        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv2.putText(frame, text, (10, height - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)

    def detect_and_track(self, vs, fps, ct):
        VideoAnalizer.detect_and_track(self, vs, fps, ct)

    def match_trackers_with_detections(self, detections, rgb, max_distance=50):
        return VideoAnalizer.match_trackers_with_detections(self, detections, rgb, max_distance=50)

    @staticmethod
    def get_y_position_of_entrance_rect(x, x_when_y_is_zero, y_when_x_is_zero):
        b = y_when_x_is_zero
        a = -y_when_x_is_zero / x_when_y_is_zero
        return int((a * x) + b)

    def has_been_above_the_line(self, centroids, x_when_y_is_zero, y_when_x_is_zero):
        return any(
            [self.get_y_position_of_entrance_rect(cx, x_when_y_is_zero, y_when_x_is_zero) > cy for cx, cy in centroids])

    def has_been_below_the_line(self, centroids, x_when_y_is_zero, y_when_x_is_zero):
        return any(
            [self.get_y_position_of_entrance_rect(cx, x_when_y_is_zero, y_when_x_is_zero) < cy for cx, cy in centroids])

    def verify_if_there_is_a_payment(self, frame):
        # verify is there was a payment
        if self.payment_frame <= self.curent_frame_number:
            self.selector.get_customer_paying(frame, self.curent_frame_number)
            self.payment_frame = self.pos.get_next_frame()
            if self.payment_frame is None:
                self.payment_frame = 10e10

    def remove_old_images(self, frame_number):
        self.selector.remove_img_didnt_leave(frame_number)

    def save_trackable_info_to_file(self):
        pass
