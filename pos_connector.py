import datetime
import time

import pandas as pd

from settings import *


class PointOfSale:
    def __init__(self):
        self.frame_customers = self.load_transctions()

    def load_transctions(self):
        """
        Function that reads the raw transactions of POS and store the frames numbers
        :return:
        """
        transactions = pd.read_csv(POS_FILENAME, encoding='latin1')
        list_time = list(set(transactions[TIME_COLUMN]))
        frame_customers = sorted([self.time_to_frame(x) for x in list_time if str(x) != 'nan'])
        return frame_customers

    @staticmethod
    def time_to_frame(transaction_time):
        """
        Function that transform the time into the frame of the video
        :return: value of the frame (int)
        """
        x = time.strptime(transaction_time, '%H:%M:%S')
        # The hour must be subtracted by the OPEN_STORE_TIME 9:00 in order to sync with the frames
        secs = datetime.timedelta(hours=x.tm_hour - OPEN_STORE_TIME, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()
        return int(secs * FRAMES_PER_SEC)

    def get_next_frame(self):
        """
        Function that pop the first value of the list
        :return:
        """
        if len(self.frame_customers) > 0:
            return self.frame_customers.pop(0)
