from collections import OrderedDict

import numpy as np
from scipy.spatial import distance as dist


class CentroidTracker:
    def __init__(self, max_disappeared=50, max_distance=50):
        """
        initialize the next unique object ID along with two ordered dictionaries used to keep track of mapping a given
        object ID to its centroid and number of consecutive frames it has been marked as "disappeared", respectively.

        :param max_disappeared: maximum consecutive frames a given object is allowed to be marked as "disappeared" until
        we need to deregister the object from tracking
        :param max_distance: maximum distance between centroids to associate an object -- if the distance is larger than
        this maximum distance we'll start to mark the object as "disappeared"
        """
        self.next_object_id = 0
        self.objects = OrderedDict()
        self.disappeared = OrderedDict()
        self.max_disappeared = max_disappeared
        self.max_distance = max_distance

    def register(self, centroid, rectangle):
        """
        when registering an object we use the next available object ID to store the centroid
        :param centroid: Tuple representing the centroid of the object
        """
        self.objects[self.next_object_id] = {'centroid': centroid, 'rectangle': rectangle}
        self.disappeared[self.next_object_id] = 0
        self.next_object_id += 1

    def deregister(self, object_id):
        """
        to deregister an object ID we delete the object ID from both of our respective dictionaries
        :param object_id: Id of the object.
        :return:
        """
        del self.objects[object_id]
        del self.disappeared[object_id]

    def update(self, rects):
        """
        
        :param rects: 
        :return: 
        """
        if len(rects) == 0:
            for object_id in list(self.disappeared.keys()):
                self.disappeared[object_id] += 1
                if self.disappeared[object_id] > self.max_disappeared:
                    self.deregister(object_id)
            return self.objects

        # initialize an array of input centroids for the current frame
        input_centroids = np.zeros((len(rects), 2), dtype="int")

        for (i, (start_x, start_y, end_x, end_y)) in enumerate(rects):
            input_centroids[i] = self.get_centroid(start_x, start_y, end_x, end_y)

        # if we are not tracking any objects take the input centroids and register each of them
        if len(self.objects) == 0:
            for i in range(0, len(input_centroids)):
                self.register(input_centroids[i], rects[i])
        # try to match the input centroids to existing object centroids
        else:
            object_ids = list(self.objects.keys())
            object_centroids = [value['centroid'] for value in list(self.objects.values())]

            dist_obj_input = dist.cdist(np.array(object_centroids), input_centroids)

            # in order to perform this matching we must (1) find the smallest value in each row and then (2) sort the
            # row indexes based on their minimum values so that the row with the smallest value as at the *front* of the
            # index list
            rows = dist_obj_input.min(axis=1).argsort()

            # next, we perform a similar process on the columns by finding the smallest value in each column and then
            # sorting using the previously computed row index list
            cols = dist_obj_input.argmin(axis=1)[rows]

            # in order to determine if we need to update, register, or deregister an object we need to keep track of
            # which of the rows and column indexes we have already examined
            used_rows = set()
            used_cols = set()

            for (row, col) in zip(rows, cols):
                if row in used_rows or col in used_cols:
                    continue

                if dist_obj_input[row, col] > self.max_distance:
                    continue

                object_id = object_ids[row]
                self.objects[object_id] = {'centroid': input_centroids[col], 'rectangle': rects[col]}
                self.disappeared[object_id] = 0

                used_rows.add(row)
                used_cols.add(col)

            unused_rows = set(range(0, dist_obj_input.shape[0])).difference(used_rows)
            unused_cols = set(range(0, dist_obj_input.shape[1])).difference(used_cols)

            if dist_obj_input.shape[0] >= dist_obj_input.shape[1]:
                for row in unused_rows:
                    object_id = object_ids[row]
                    self.disappeared[object_id] += 1
                    if self.disappeared[object_id] > self.max_disappeared:
                        self.deregister(object_id)
            else:
                for col in unused_cols:
                    self.register(input_centroids[col], rects[col])

        return self.objects

    def get_centroid(self, start_x, start_y, end_x, end_y):
        return int((start_x + end_x) / 2.0), int(end_y)


class EntranceCentroidTracker(CentroidTracker):
    def get_centroid(self, start_x, start_y, end_x, end_y):
        return int((start_x + end_x) / 2.0), int((start_y + end_y) / 2.0)
