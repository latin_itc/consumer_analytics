class Trackable:
    def __init__(self, objectID, centroid):
        # store the object ID, then initialize a list of centroids using the current centroid
        self.objectID = objectID
        self.centroids = [centroid]  # it is a list because it will contain an object’s centroid location history.
        self.frames = []

        # initialize a boolean used to indicate if the object has already been counted or not
        self.counted_in = False
        self.counted_out = False
