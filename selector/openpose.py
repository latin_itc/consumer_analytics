import cv2


class OpenPose():
    """Class to use openpose algorithm. First fit and then call get_points.
    The types of modes are COCO or MPI"""

    def __init__(self, mode="COCO", threshold=0.1):
        self.image = None
        self.net = None
        self.mode = mode
        self.frameWidth = None
        self.frameHeight = None
        self.threshold = threshold

    def fit(self, image):
        self.image = image
        self.frameWidth = self.image.shape[1]
        self.frameHeight = self.image.shape[0]
        self.threshold = 0.1

    def get_points(self):
        self.load_nn()
        net = self.net
        output = net.forward()

        H = output.shape[2]
        W = output.shape[3]

        # Empty list to store the detected keypoints
        points = []

        for i in range(self.nPoints):
            # confidence map of corresponding body's part.
            probMap = output[0, i, :, :]

            # Find global maxima of the probMap.
            minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

            # Scale the point to fit on the original image
            x = (self.frameWidth * point[0]) / W
            y = (self.frameHeight * point[1]) / H

            if prob > self.threshold:
                # Add the point to the list if the probability is greater than the threshold
                points.append((int(x), int(y)))
            else:
                points.append(None)

        return points

    def load_nn(self):
        base_path = r"C:\Users\Felipe\OneDrive\Python\learnopencv\OpenPose"
        if self.mode is "COCO":
            protoFile = os.path.join(base_path, "pose/coco/pose_deploy_linevec.prototxt")
            weightsFile = os.path.join(base_path, "pose/coco/pose_iter_440000.caffemodel")
            self.nPoints = 18
            POSE_PAIRS = [[1, 0], [1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7], [1, 8], [8, 9], [9, 10], [1, 11],
                          [11, 12], [12, 13], [0, 14], [0, 15], [14, 16], [15, 17]]

        elif self.mode is "MPI":
            protoFile = os.path.join(base_path, "pose/mpi/pose_deploy_linevec_faster_4_stages.prototxt")
            weightsFile = os.path.join(base_path, "pose/mpi/pose_iter_160000.caffemodel")
            self.nPoints = 15
            POSE_PAIRS = [[0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 14], [14, 8], [8, 9], [9, 10],
                          [14, 11], [11, 12], [12, 13]]

        net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)
        # input image dimensions for the network
        inWidth = 368
        inHeight = 368
        inpBlob = cv2.dnn.blobFromImage(self.image, 1.0 / 255, (inWidth, inHeight),
                                        (0, 0, 0), swapRB=False, crop=False)
        net.setInput(inpBlob)
        self.net = net
