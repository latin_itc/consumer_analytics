import cv2
import numpy as np

from detection.customer_detector import Customer
from detection.yolo_detector import YOLOPersonDetector
from settings import *
import glob
from shutil import copyfile
import logging

logging.basicConfig(format='%(asctime)s;%(name)s;%(levelname)s;%(message)s',
                    level=logging.INFO, handlers=[
                    logging.FileHandler("customer_analytics.log"),
                    logging.StreamHandler()])
logger = logging.getLogger(__name__)
logger.info("starting")

class CustomerSelector:
    """

    :param frame: (array) image to be used
    :param position: (str) choose between entering/leaving/paying
    """

    def __init__(self):
        self.customer = Customer()
        self.yolo = YOLOPersonDetector(YOLO_CONFIG, YOLO_WEIGHTS, YOLO_CLASSES, 0)

    def get_customer_entering(self, frame, box, frame_number):
        customer_frame = self.crop_box(box, frame)
        self.customer.add_customer(customer_frame, frame_number)
        logger.info('add customer;{}'.format(frame_number))

    def get_customer_leaving(self, frame, box, frame_number):
        customer_frame = self.crop_box(box, frame)
        self.save_image(customer_frame, frame_number, FILE_PATH_CUSTOMERS_LEAVING)
        self.customer.customer_session(customer_frame, frame_number)
        logger.info('customer leaving;{}'.format(frame_number))

    def get_customer_paying(self, frame, frame_number):
        position_box = self.get_position_box(frame, POSITION_PAYING)
        reference_point = np.array(CASHIER_POINT) * frame.shape[-2::-1]

        customer_frame = self.get_customer_frame(frame, position_box, reference_point)

        self.save_image(customer_frame, frame_number, FILE_PATH_CUSTOMERS_PAYING)

        logger.info('customer paying;{}'.format(frame_number))
        match_customer_frame = self.customer.matching(customer_frame)
        self.customer.customer_bought(match_customer_frame)


    def get_customer_frame(self, frame, position_box, reference_point):
        # crop base image with dimensions above
        cropped_frame = self.crop_box(position_box, frame)
        # apply yolo
        yolo_boxes = self.yolo.predict(cropped_frame)
        # crop box
        print("NUMERO BOXES: {}".format(len(yolo_boxes)))
        if len(yolo_boxes) >= 1:
            # Get people closest to the point of interest
            corect_box = self.closest_person_to_point(yolo_boxes, reference_point)
            customer_frame = self.crop_box(corect_box, cropped_frame)
        else:
            print("DIDN'T RECOGNIZE ANYONE - returning")
            # try again
            return

        return customer_frame

    def get_position_box(self, frame, position):
        y, x = frame.shape[0], frame.shape[1]
        box = np.array(position) * [x, y, x, y]
        return box.astype(np.int32)

    def resize_images(self, im1, im2):
        """Returns both images, but before:
        - make the one with the biggest lenght proportional smaller to match the others lenght
        - with the proportional images, crop the one with the larger height"""
        # makes the lenght equals by making the biggest image proportional smaller
        if im1.shape[1] > im2.shape[1]:
            bigger_lenght, smaller_lenght = im1, im2
        else:
            bigger_lenght, smaller_lenght = im2, im1

        proportional_diff = smaller_lenght.shape[1] / bigger_lenght.shape[1]
        bigger_lenght = cv2.resize(bigger_lenght, None, fx=proportional_diff, fy=proportional_diff)
        # crops the botton part of the biggest image
        min_height = min(bigger_lenght.shape[0], smaller_lenght.shape[0])
        return bigger_lenght[:min_height, :, :], smaller_lenght[:min_height, :, :]

    def crop_box(self, box, frame):
        return frame[box[1]:box[3], box[0]:box[2]]

    def closest_person_to_point(self, boxes, point):
        """
        Returns the box closest to the point of interest
        """
        distances = []
        for box in boxes:
            center_box = (box[0] + box[2]) / 2.0, (box[1] + box[3]) / 2.0
            distances.append(np.linalg.norm(np.array(center_box) - np.array(point)))
        return boxes[np.array(distances).argmin()]

    def remove_img_didnt_leave(self, actual_frame_number):
        for filename in glob.glob(os.path.join(FILE_PATH_CUSTOMERS, '*.png')):
            old_image_frame_number = int(os.path.basename(filename).split(".")[0])
            seconds = abs(int(((actual_frame_number - old_image_frame_number) / FRAMES_PER_SEC)))
            if seconds > SECONDS_TO_REMOVE_IMAGE:
                # copy file
                copyfile(filename, os.path.join(FILE_PATH_CUSTOMERS_LEFT, str(old_image_frame_number) + '.png'))
                # remove file
                os.remove(filename)

    def save_image(self, image, frame_number, path):
        filename = str(frame_number) + ".png"
        file = os.path.join(path, filename)
        cv2.imwrite(file, image)
