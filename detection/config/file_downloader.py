import math

import requests
from tqdm import tqdm

URL_WEIGHTS = 'https://pjreddie.com/media/files/yolov3.weights'
FILENAME = './yolov3.weights'

r = requests.get(URL_WEIGHTS, stream=True)

# Total size in bytes.
total_size = int(r.headers.get('content-length', 0))
block_size = 1024
wrote = 0
with open(FILENAME, 'wb') as f:
    for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size // block_size), unit='KB', unit_scale=True):
        wrote = wrote + len(data)
        f.write(data)

if total_size != 0 and wrote != total_size:
    print("ERROR, something went wrong")
