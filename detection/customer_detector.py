import datetime
import glob
import ntpath
import operator
from shutil import copyfile
import cv2
from skimage.measure import compare_ssim as ssim
import logging
from settings import *

logger = logging.getLogger()

CUSTOMER_IN = 0


class Customer:
    def __init__(self):
        self.customers = []
        self.customers_sessions_list = {}
        self.purchases = {}

    def add_customer(self, image, frame_number):
        """

        :param type_list:
        :return:
        """
        filname = str(frame_number) + ".png"
        file = os.path.join(FILE_PATH_CUSTOMERS, filname)
        cv2.imwrite(file, image)
        self.customers.append(frame_number)
        self.purchases[frame_number] = False

    def get_customer(self, frame_number):
        """

        :param type_path:
        :param frame_number:
        :return:
        """
        filname = str(frame_number) + ".png"
        file = os.path.join(FILE_PATH_CUSTOMERS, filname)
        return cv2.imread(file)

    def remove_customer(self, frame_number):
        """

        :return:
        """
        self.customers.remove(frame_number)
        filename = str(frame_number) + ".png"
        file = os.path.join(FILE_PATH_CUSTOMERS, filename)
        copyfile(file, os.path.join(FILE_PATH_CUSTOMERS_LEFT, filename))
        os.remove(file)

    def customer_session(self, img, frame_number):
        """

        :param img:
        :param frame_number:
        :return:
        """
        frame_match = self.matching(img)
        customer_time = self.time_session(frame_number, frame_match)
        self.remove_customer(frame_match)
        self.customers_sessions_list[frame_match] = [self.purchases[frame_match], customer_time]
        self.write_time_session([frame_match, self.purchases[frame_match], customer_time])


    def write_time_session(self, customer_session):
        with open('customer_sessions.csv', 'a') as f:
            f.write("{}\n".format(";".join([str(value) for value in customer_session])))

    def time_session(self, frame_number, frame_matched):
        """

        :param frame_number:
        :param frame_matched:
        :return:
        """
        sec = datetime.timedelta(seconds=abs(int(((frame_matched - frame_number) / FRAMES_PER_SEC))))
        session = datetime.datetime(1, 1, 1) + sec
        return session.hour, session.minute, session.second

    def matching(self, image):
        """

        :param image:
        :return:
        """
        files_ranking = {}
        for filename in glob.glob(os.path.join(FILE_PATH_CUSTOMERS, '*.png')):
            image_to_compare = int(ntpath.basename(filename).split(".")[0])
            ranking = self.ranking(image, filename)
            files_ranking[image_to_compare] = ranking

        frame_match = max(files_ranking.items(), key=operator.itemgetter(1))[0]
        logger.info('matched above with;{}'.format(frame_match))
        return frame_match

    def ranking(self, image1, image2):
        """
        Calculates the similarity between a front-facing and back-facing image by
        flipping the back-facing image and finally calculating the structural similarity index.
        :param image1: image, which is the back-facing one.
        :param image2: Path of the second image, which is the front-facing one.
        :return: The similarity as given by structural similarity index.
        """
        im2 = cv2.imread(image2)
        target_resized, input_resized = self.resize_images(image1, im2)
        target_mirrored = cv2.flip(target_resized, 1)
        return ssim(target_mirrored, input_resized, multichannel=True)

    @staticmethod
    def resize_images(im1, im2):
        """Returns both images, but before:
        - make the one with the biggest length proportional smaller to match the others length
        - with the proportional images, crop the one with the larger height"""
        # makes the lenght equals by making the biggest image proportional smaller
        if im1.shape[1] > im2.shape[1]:
            bigger_length, smaller_length = im1, im2
        else:
            bigger_length, smaller_length = im2, im1

        proportional_diff = smaller_length.shape[1] / bigger_length.shape[1]
        bigger_lenght = cv2.resize(bigger_length, None, fx=proportional_diff, fy=proportional_diff)
        # crops the botton part of the biggest image
        min_height = min(bigger_lenght.shape[0], smaller_length.shape[0])
        return bigger_lenght[:min_height, :, :], smaller_length[:min_height, :, :]

    def customer_bought(self, frame_number):
        self.purchases[frame_number] = True
