import cv2
import numpy as np


class YOLOPersonDetector:
    """
    You only live(look) once.
    Inspired by: https://www.arunponnusamy.com/yolo-object-detection-opencv-python.html
    """

    def __init__(self, config_path, weights_path, classes_path, conf_threshold=0.5, nms_threshold=0.5):
        self.classes = self.assemble_classes(classes_path)
        self.net = cv2.dnn.readNet(weights_path, config_path)
        self.detection_threshold = conf_threshold
        self.nms_threshold = nms_threshold

    @staticmethod
    def assemble_classes(classes_path):
        with open(classes_path, 'r') as f:
            classes = [line.strip() for line in f.readlines()]
        return classes

    def _get_output_layers(self):
        layer_names = self.net.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
        return output_layers

    def forward(self, image, scale=0.00392, size=(416, 416), mean=(0, 0, 0)):
        blob = cv2.dnn.blobFromImage(image, scale, size, mean, True, crop=False)
        self.net.setInput(blob)
        outs = self.net.forward(self._get_output_layers())
        return outs

    def assemble_prediction_data(self, outs, width, height):
        class_ids = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.25 and class_id == 0:
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])
        indices = cv2.dnn.NMSBoxes(boxes, confidences, self.detection_threshold, self.nms_threshold)
        return confidences, class_ids, indices, boxes

    @staticmethod
    def get_bounding_boxes(indices, boxes):
        b_boxes = []
        for i in indices:
            i = i[0]
            box = boxes[i]
            x, y, w, h = box[0], box[1], box[2], box[3]
            b_boxes.append((round(x), round(y), round(x + w), round(y + h)))
        return b_boxes

    def predict(self, image):
        res = self.forward(image)
        width = image.shape[1]
        height = image.shape[0]
        confidences, class_ids, indices, boxes = self.assemble_prediction_data(res, width, height)
        b_boxes = self.get_bounding_boxes(indices, boxes)
        return b_boxes
