import os

YOLO_CONFIG = os.path.join(os.getcwd(), "detection", "config", "yolov3.cfg")
YOLO_CLASSES = os.path.join(os.getcwd(), "detection", "config", "yolov3.txt")
YOLO_WEIGHTS = os.path.join(os.getcwd(), "detection", "config", "yolov3.weights")
CENTROIDS_OUTPUT_FILENAME = os.path.join(os.getcwd(), "output", "centroids.txt")

POS_FILENAME = os.path.join(os.getcwd(), "input", "pos-df-9.csv")
FRAMES_PER_SEC = 15
OPEN_STORE_TIME = 9
TIME_COLUMN = "Hora"

DEFAULT_FRAMES = 60
DEFAULT_FRAME_START = 0
DEFAULT_FRAME_END = 1350
LABEL_FILE_REGEX = "frame_[0-9]*.txt"
IMG_FILE_REGEX = "frame_[0-9]*.png"
IOU_THRESHOLD = 0.5

SECONDS_TO_REMOVE_IMAGE = 30 * 60

FILE_PATH_CUSTOMERS = os.path.join(os.getcwd(), "detection", "customers_in")
FILE_PATH_CUSTOMERS_LEFT = os.path.join(os.getcwd(), "detection", "customers_left")
FILE_PATH_CUSTOMERS_PAYING = os.path.join(os.getcwd(), "detection", "customers_paying")
FILE_PATH_CUSTOMERS_LEAVING = os.path.join(os.getcwd(), "detection", "customers_leaving")

if not os.path.isdir(FILE_PATH_CUSTOMERS):
    os.mkdir(FILE_PATH_CUSTOMERS)
if not os.path.isdir(FILE_PATH_CUSTOMERS_LEFT):
    os.mkdir(FILE_PATH_CUSTOMERS_LEFT)
if not os.path.isdir(FILE_PATH_CUSTOMERS_LEAVING):
    os.mkdir(FILE_PATH_CUSTOMERS_LEAVING)
if not os.path.isdir(FILE_PATH_CUSTOMERS_PAYING):
    os.mkdir(FILE_PATH_CUSTOMERS_PAYING)


IMAGES_FOLDER = os.path.join(os.getcwd(), 'videos', 'images')
POSITION_IN = (400 / 1280.0, 30 / 720.0, 830 / 1280.0, 390 / 720.0)
POSITION_OUT = POSITION_IN
POSITION_PAYING = (400 / 1280.0, 200 / 720.0, 715 / 1280.0, 575 / 720.0)
ENTRANCE_POINT = (585 / 1280.0, 245 / 720.0)
CASHIER_POINT = (500 / 1280.0, 500 / 720.0)
