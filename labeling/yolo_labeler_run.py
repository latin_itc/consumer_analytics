import json
import logging
import os
import sys

from labeling.labeler_argparse import LabelerArgumentParserBuilder
from labeling.yolo_labeler import YOLOLabeler

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


def run_yolo_labeler():
    """
    Assembles the argument parser for the YOLO Labeler and runs it over the given input directory. If out_dir is specified
    the results are written into a .csv file as well.
    :return: A DataFrame containing the labels (bounding boxes) predicted by the YOLO detector.
    """
    args = LabelerArgumentParserBuilder(
        description="Run YOLO against a directory of images or frames").assemble_yolo_labeler_parser() \
        .assemble_yolo_parser().parser.parse_args()
    argdict = vars(args)
    img_dir = argdict["img_dir"]
    out_dir = argdict["outdir"]
    yolo_cfg = argdict["detectorcfg"]
    yolo_weights = argdict["detectorweights"]
    yolo_classes = argdict["detectorclasses"]
    confidence_threshold = argdict["confidencethrs"]
    nms_threshold = argdict["nmsthrs"]
    labeler = YOLOLabeler(img_dir, yolo_cfg, yolo_weights, yolo_classes, confidence_threshold, nms_threshold)
    bounding_boxes = labeler.label_images()
    if out_dir:
        bounding_boxes.to_csv(out_dir, index=False)
        logger.info(f"Wrote YOLO results to: {out_dir}")
        metadata = {"yolo_threshold": confidence_threshold, "nms_threshold": nms_threshold}
        basename = os.path.basename(out_dir)
        filename = os.path.splitext(basename)[0]
        metadata_name = filename + "_metadata.json"
        parent_path = os.path.dirname(out_dir)
        metadata_path = os.path.join(parent_path, metadata_name)
        with open(metadata_path, "w") as f:
            json.dump(metadata, f)
            logger.info(f"Wrote metadata to: {metadata_path}")
    return bounding_boxes


if __name__ == "__main__":
    run_yolo_labeler()
