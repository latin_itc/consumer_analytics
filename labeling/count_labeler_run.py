from labeling.count_labeler import CountLabeler
from labeling.labeler_argparse import LabelerArgumentParserBuilder


def run_count_labeler():
    """
    Parses the arguments and runs the count labeler.
    """
    args = LabelerArgumentParserBuilder(
        description="Run human or YOLO labelling.").assemble_yolo_parser().assemble_count_parser().parser.parse_args()
    argdict = vars(args)
    output_path = argdict["output"]
    skip_frames = argdict["frames"]
    input_path = argdict["input"]
    nn_counter = argdict["nncounter"]
    count_people = argdict["countpeople"]
    detector_cfg_path = argdict["detectorcfg"]
    detector_weights_path = argdict["detectorweights"]
    detector_classes_path = argdict["detectorclasses"]
    confidence_threshold = argdict["confidencethrs"]
    nms_threshold = argdict["nmsthrs"]
    count_labeler = CountLabeler(output_path, skip_frames, input_path, nn_counter, count_people, detector_cfg_path,
                                 detector_weights_path, detector_classes_path, confidence_threshold, nms_threshold)
    count_labeler.run()


if __name__ == "__main__":
    run_count_labeler()
