import argparse
import os

from settings.settings import YOLO_CONFIG, YOLO_WEIGHTS, YOLO_CLASSES, DEFAULT_FRAMES, IOU_THRESHOLD, \
    DEFAULT_FRAME_START, DEFAULT_FRAME_END

DEFAULT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "output")


class LabelerArgumentParserBuilder:
    """
    A generic Builder-like design pattern class that builds an argument with the pre-defined arguments within each component, which
    is defined as a method of the function.
    """

    def __init__(self, description):
        """
        Main initiaizatlion function, which uses a description in order to assemble the Argument Parser that will be
        eventually returned.
        :param description: The description of the program that will be eecuted.
        """
        self.parser = argparse.ArgumentParser(description)

    def assemble_yolo_parser(self):
        """
        Assembles a generic parser for the configuration of the YOLO detector.
        :return: Itself.
        """
        self.parser.add_argument('--detectorcfg', type=str, help="Path to the config of the detector",
                                 default=YOLO_CONFIG)
        self.parser.add_argument('--detectorweights', type=str, help="Path to the weights of the detector",
                                 default=YOLO_WEIGHTS)
        self.parser.add_argument('--detectorclasses', type=str, help="Path to the classes of the detector",
                                 default=YOLO_CLASSES)
        self.parser.add_argument('--confidencethrs', type=float, help="Confidence threshold for the deteector",
                                 default=0.5)
        self.parser.add_argument('--nmsthrs', type=float, help="Non max suppresion threshold for the deteector",
                                 default=0.5)
        return self

    def assemble_yolo_labeler_parser(self):
        """
        Assembles a generic parser for the YOLO Labeler class.
        :return: Itself.
        """
        self.parser.add_argument('img_dir', type=str, help="The directory of the images to run through YOLO.")
        self.parser.add_argument('--outdir', type=str, help="The directory to output the corresponding labels.")
        return self

    def assemble_iou_metrics_parser(self):
        """
        Assembles a generic parser for the IoU metrics functionality.
        :return: Itself.
        """
        self.parser.add_argument('truth_dir', type=str,
                                 help="The path to the directory that contains the ground truth bounding boxes.")
        self.parser.add_argument('pred_file', type=str,
                                 help="The path to the file that contains the prediction's bounding boxes.")
        self.parser.add_argument('output_file', type=str, help="The path to which to output the results as a .csv")
        self.parser.add_argument('image_width', type=int, help="The width of the images")
        self.parser.add_argument('image_height', type=int, help="The height of the images")
        self.parser.add_argument('--iou', type=float, help="The IoU threshold to use.", default=IOU_THRESHOLD)
        return self

    def assemble_count_parser(self):
        """
        Assembles a parser for the given Labeling Counter functionality.
        :return: Itself.
        """
        self.parser.add_argument('input', type=str, help="The path of the input video")
        self.parser.add_argument('--output', type=str, help="The path of the file with the labelled frames",
                                 default=DEFAULT_PATH)
        self.parser.add_argument('--frames', type=int, help="The amount of frames to skip for every step of labeling",
                                 default=DEFAULT_FRAMES)
        self.parser.add_argument('--nncounter', action="store_true",
                                 help="Run the neural network alongside and add its results", default=False)
        self.parser.add_argument('--countpeople', action="store_true",
                                 help="Whether to count people or not, defaults to True", default=False)
        return self

    def assemble_frame_generator_parser(self):
        """
        Assembles an argument parser for the Frame Generator functionality.
        :return: Itself.
        """
        self.parser.add_argument('input', type=str, help="The path of the input video")
        self.parser.add_argument('output', type=str, help="The path to output the images")
        self.parser.add_argument('--frames', type=int, help="The amount of frames to skip for every image save",
                                 default=DEFAULT_FRAMES)
        self.parser.add_argument('--startframe', type=int, help="The frame to start on", default=DEFAULT_FRAME_START)
        self.parser.add_argument('--endframe', type=int, help="The amount of frames to generate",
                                 default=DEFAULT_FRAME_END)
        return self
