import logging
import os
import sys
from datetime import datetime

import cv2
import pandas as pd

from labeling.utils.util_funcs import validate_and_create_output_folder, create_detector
from labeling.utils.video_streamer import VideoStreamer

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


class CountLabeler:
    """
    Class for the counting functionality, which consists of the user defining how many objects (persons in this case) it detects
    alongside YOLO if specified.
    """

    def __init__(self, output_path, skip_frames, input_file, nn_counter=False, count_people=True,
                 detector_cfg_path=None, detector_weights_path=None, detector_classes_path=None,
                 confidence_thrs=0.5, nms_thrs=0.5):

        self.output_path = output_path
        self.skip_frames = skip_frames
        self.input_file = input_file
        self.nn_counter = nn_counter
        self.count_people = count_people
        self._parse_labeling_inputs(count_people, nn_counter)
        if self.nn_counter:
            self.detector = create_detector(detector_cfg_path, detector_weights_path, detector_classes_path,
                                            confidence_thrs, nms_thrs)
        else:
            self.detector = None

    @staticmethod
    def _parse_labeling_inputs(count_people, nn_counter):
        """
        Makes sure that the counter is either using YOLO, human input or both. Raises an exception if it's using neither.
        :param count_people: The human input counter flag.
        :param nn_counter: The YOLO counter flag.
        """
        if not count_people and not nn_counter:
            raise ValueError("You either must use the detector or count people.")

    def _run_detection_labeling(self, frame, people_counter_per_frame, people_count, frame_counter):
        """
        Appends to the people_counter_per_frame list the people that has been seen upon the prediction of a frame.
        :param frame: The current frame as a matrix.
        :param people_counter_per_frame: The list of counts.
        :param people_count: The people count defined by a human previously.
        :param frame_counter: The current frame the labeling procedure is in.
        """
        # This has a side effect
        detections = self.detector.predict(frame)
        detections_count = len(detections)
        logger.info(f"Detector sees: {detections_count}")
        if self.count_people:
            people_counter_per_frame.append(
                ({"frame": frame_counter, "people_count": people_count,
                  "detector_people_count": detections_count}))
        else:
            people_counter_per_frame.append(
                ({"frame": frame_counter,
                  "detector_people_count": detections_count}))

    @staticmethod
    def _run_human_labeling():
        """
        Prompts for human input to see how many people a human has seen.
        :return: An integer representation the amount of information detected by a human.
        """
        people_count = input("How many people do you see?\n")
        while not people_count.isnumeric():
            people_count = input("How many people do you see?")
        return int(people_count)

    def run_labeling_procedure(self):
        """
        Runs over the whole input file (a video or image stream) and prompts for
        human input and runs YOLO alongside if specified by the corresponding flags.
        :return:
        """
        streamer = VideoStreamer(self.input_file)
        frame_counter = 0
        people_counter_per_frame = []
        for frame in streamer.stream_video():
            if (frame_counter % self.skip_frames) == 0:
                frame = cv2.resize(frame, (640, 480))
                people_count = 0
                cv2.imshow("video", frame)
                cv2.moveWindow("video", 0, 0)
                if self.count_people:
                    logger.info("### Human Labelling Phase ###")
                    self._run_human_labeling()
                if self.detector:
                    self._run_detection_labeling(frame, people_counter_per_frame, people_count, frame_counter)
                else:
                    people_counter_per_frame.append(({"frame": frame_counter, "people_count": people_count}))
            frame_counter += 1
        return people_counter_per_frame

    def run(self):
        """
        Main input for the function. Runs the labeling procedure and afterwards writes the results into a specific by
        argument parameter .csv file.
        """
        logger.info("Input Path: {}".format(self.input_file))
        logger.info("Output Path: {}".format(self.output_path))
        logger.info("Frames to skip per label phase: {}".format(self.skip_frames))
        validate_and_create_output_folder(self.output_path)
        raw_label_data = self.run_labeling_procedure()
        df = pd.DataFrame(raw_label_data)
        current_date = datetime.now().strftime("%Y-%m-%d_%H-%M")
        output_path = None
        if self.count_people:
            output_path = os.path.join(self.output_path, "labels-" + current_date + ".csv")
        if not self.count_people and self.detector:
            output_path = os.path.join(self.output_path, "labels-det-only-" + current_date + ".csv")
        logger.info(f"Writing to: {output_path}")
        df.to_csv(output_path)
