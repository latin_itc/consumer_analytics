import glob
import logging
import os
import sys

import cv2
import pandas as pd

from labeling.utils.util_funcs import create_detector
from settings.settings import YOLO_CLASSES, YOLO_WEIGHTS, YOLO_CONFIG, IMG_FILE_REGEX

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


class YOLOLabeler:
    """
    Runs the YOLO detector over a given input folder.
    """

    def __init__(self, img_dir, detector_cfg_path=YOLO_CONFIG, detector_weights_path=YOLO_WEIGHTS,
                 detector_classes_path=YOLO_CLASSES, confidence_thrs=0.5, nms_thrs=0.5, file_regex=IMG_FILE_REGEX):
        self.detector = create_detector(detector_cfg_path, detector_weights_path, detector_classes_path,
                                        confidence_thrs, nms_thrs)
        self.image_paths = glob.glob(os.path.join(img_dir, file_regex))

    def read_images(self):
        """
        Reads the images from a directory and returns them as a stream along with their frame number.
        :return: A generator of tuples (frame, image) where the frame is the current number corresponding
        to the frame of the video or image stream.
        """
        logger.info(f"Total images: {len(self.image_paths)}")
        for image_path in self.image_paths:
            try:
                img = cv2.imread(image_path)
                filename = os.path.splitext(os.path.basename(image_path))[0]
                frame = filename[filename.find('_') + 1:]
                yield (frame, img)
            except Exception as e:
                logger.error(f"An error has occured: {e}")
                yield None

    def label_images(self):
        """
        Labels the images by detecting the objects using the YOLO detector.
        :return: A DataFrame with the prediction for each frame.
        """
        logger.info("Starting YOLO Labeler")
        results = []
        for frame, image in self.read_images():
            logger.info(f"Running YOLO over frame: {frame}")
            bboxes = self.detector.predict(image)
            for bbox in bboxes:
                x1, y1, x2, y2 = bbox
                res_dict = {"frame": frame, "x1": int(x1), "y1": int(y1), "x2": int(x2), "y2": int(y2)}
                results.append(res_dict)
        logger.info("YOLO Labeler finished")
        return pd.DataFrame(results)
