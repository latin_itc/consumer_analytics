import logging
import sys

import pandas as pd

from labeling.labeler_argparse import LabelerArgumentParserBuilder
from labeling.utils.relative_converter import RelativeConverter
from settings.settings import IOU_THRESHOLD

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


def bb_intersection_over_union(boxA, boxB):
    """
    Calculates the intersection of union between two bounding boxes, where the highest number is 1 if the intersection
    is an exact fit and 0 if there's absolutely no intersection between the union of the two bounding boxes.
    :param boxA: The first bounding box.
    :param boxB: The second bounding box.
    :return: A floating pointer number between 0 and 1 indicating the amount of intersection between the union of both bounding boxes
    if they're within eachother.
    """
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


def _aggregate_dictionary_results(dict1, dict2):
    """
    Aggregates the values of the keys of two dictionaries.
    :param dict1: The first dictionary.
    :param dict2: The second dictionary
    :return: Aggregated dictionary
    """
    return {k: dict1[k] + dict2[k] for k in set(dict1) & set(dict2)}


def calculate_metrics_over_common_indices(truths, predictions, indices, iou_threshold=0.5):
    """
    Calculates True Positives, False Positives and False Negatives between the Ground Truth and the Predictions
    of an object detection system over the indices which represent the frames of a stream (video). These metrics
    are calculated by using the intersection of union which is a method for defining true positives, false positives
    and false negatives over several objects detected in an image.
    :param truths: The ground truth as a pandas DataFrame.
    :param predictions: The predictions as a pandas DataFrame.
    :param indices: The indices or frames we'll iterate over.
    :param iou_threshold: The threshold that will be used to defined True Positives. If the intersection of union (IoU)
    is above this threshold, then it will be counted as a True Positive, otherwise it will be counted as a False Positive.
    :return: A dictionary containing the True Positive, False Positive and False Negative count.
    """
    pred_results = {"tp": 0, "fp": 0, "fn": 0}

    for frame in indices:
        # Get each bounding box of the ground truth and the predictions as a list
        ground_truths = truths.loc[frame][["x1", "y1", "x2", "y2"]].values.tolist()
        yolo_preds = predictions.loc[frame][["x1", "y1", "x2", "y2"]].values.tolist()
        # We're using this list as a lookup for the ground truths that were not covered by the predictions.
        leftover_truths = ground_truths.copy()

        for truth in ground_truths:
            if not yolo_preds:
                # We're done here!
                break
            # The maximum IoU will be the best fit for our ground truth.
            yolo_pred, max_iou = max([(yolo_pred, bb_intersection_over_union(truth, yolo_pred))
                                      for yolo_pred
                                      in yolo_preds], key=lambda x: x[1])
            if max_iou > iou_threshold:
                # If it's greater than the threshold, we take it as a true positive.
                pred_results["tp"] += 1
            else:
                # If it's not, we take it as a false positive.
                pred_results["fp"] += 1
            # We remove from the predictions as it's already been accounted for.
            yolo_preds.remove(yolo_pred)
            # We remove from the truths as the truths that are left over are false negatives, as seen below.
            leftover_truths.remove(truth)

        pred_results["fn"] += len(leftover_truths)
    return pred_results


def calculate_metrics_over_missing_indices(truths, predictions, missing_indices):
    """
    Defines the missing metrics (false positives and false negatives) over the missing indices that YOLO
    may not have found given a bad detection streak.
    :param truths: A DataFrame containing the ground truths.
    :param predictions: The Predictions YOLO made.
    :param missing_indices: The indices that we're going to look into.
    :return: A Dictionary containing the FP and FN calculated over missing values, it also includes TP as
    another function aggregates dictionaries with the same keys.
    """
    pred_results = {"fp": 0, "fn": 0, "tp": 0}
    for frame in missing_indices:
        if frame not in truths.index:
            # If they're not part of the ground truth then they're false positives.
            yolo_preds = predictions.loc[frame]
            pred_results["fp"] += len(yolo_preds)
        else:
            # And if they are, they are false negatvies.
            ground_truths = truths.loc[frame]
            pred_results["fn"] += len(ground_truths)
    return pred_results


def calculate_metrics(truths, predictions, iou_threshold=IOU_THRESHOLD):
    """
    Given a DataFrame of truths and predictions, calculates the intersection of union between each prediction over each frame
    and its corresponding ground truth given the intersection of union threshold, returns metrics derived from these calculations.
    :param truths: A DataFrame containing the ground truths as bounding boxes.
    :param predictions: A DataFrame containing predictions as bounding boxes.
    :param iou_threshold: The Intersection of Union threshold over which True Positives will be defined (it will not be counted as a
    True Positive if it is below).
    :return: A DataFrame containing the True Positives, False Positives, False Negatives, Accuracy Score, Precision and Recall
    """
    common_indices = set(truths.index.intersection(predictions.index))
    # Sometimes things will not be in YOLO.
    missing_indices = set(truths.index.symmetric_difference(predictions.index))
    pred_results = {"tp": 0, "fp": 0, "fn": 0}

    results = calculate_metrics_over_common_indices(truths, predictions, common_indices, iou_threshold)
    pred_results = _aggregate_dictionary_results(pred_results, results)

    missing_indices_results = calculate_metrics_over_missing_indices(truths, predictions, missing_indices)
    pred_results = _aggregate_dictionary_results(pred_results, missing_indices_results)
    assert sum(pred_results.values()) == len(truths)
    tp, fp, fn = pred_results["tp"], pred_results["fp"], pred_results["fn"]
    accuracy = tp / len(truths)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    logger.info(f"Accuracy: {accuracy:.3f}")
    logger.info(f"Precision: {precision:.3f}")
    logger.info(f"Recall: {recall:.3f}")
    return pd.DataFrame({"tp": tp, "fp": fp, "fn": fn, "accuracy": accuracy, "precision": precision,
                         "recall": recall}, index=[0])


def run_iou_metrics():
    """
    Main entry of the class. Parses the arguments and calculates the metrics for the given input and output dataframes using
    a specific intersection of union threshold, which is afterwards written into a parameterized output path as a comma separated
    value (csv) file containing metrics derived from these calculations.
    """
    args = LabelerArgumentParserBuilder(
        description="Calculate the IoU over the ground truths compared to YOLO").assemble_iou_metrics_parser().parser.parse_args()
    logger.info("Converting ground truth to absolute bounding boxes")
    converter = RelativeConverter(args.truth_dir, args.image_width, args.image_height)
    truths = converter.yolo_labels_to_bbox()
    logger.info("Reading predictions")
    predictions = pd.read_csv(args.pred_file, index_col="frame")
    logger.info("Calculating IoUs for detections")
    metrics = calculate_metrics(truths, predictions)
    metrics.to_csv(args.output_file, index=False)
    logger.info("Done!")


if __name__ == "__main__":
    run_iou_metrics()
