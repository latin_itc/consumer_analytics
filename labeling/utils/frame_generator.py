import logging
import os
import sys

import cv2

from labeling.labeler_argparse import LabelerArgumentParserBuilder
from labeling.utils.video_streamer import VideoStreamer

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


class FrameGenerator:
    """
    Class that generates a stream of frames for a given interval of a stream, namely, a video.
    """

    def _run_parser(self):
        """
        Runs the argument parser for the Frame Generator.
        :return: A dictionary of the given arguments.
        """
        parser = LabelerArgumentParserBuilder(description="Generate the images for each frame for labeling")
        args = parser.assemble_frame_generator_parser().parser.parse_args()
        argdict = dict()
        argdict["input_path"] = args.input
        argdict["output_path"] = args.output
        argdict["skip_frames"] = args.frames
        argdict["end_frame"] = args.endframe
        argdict["start_frame"] = args.startframe
        logger.info(
            f"Parameters: input_path {args.input} -- output_path: {args.output} -- frames_to_skip: {args.frames} -- end_frame {args.endframe} -- start_frame {args.startframe}")
        return argdict

    def run(self):
        """
        Calculates and generates the frames from a video, which are written into a given output directory.
        :return:
        """
        args = self._run_parser()
        stream = VideoStreamer(args["input_path"])
        from labeling.utils.util_funcs import validate_and_create_output_folder
        validate_and_create_output_folder(args["output_path"])
        frames_to_skip = args["skip_frames"]
        for i, frame in enumerate(stream.stream_video()):
            if i == args["start_frame"] + args["end_frame"] + 1:
                stream.end_stream()
                break
            if i >= args["start_frame"] and i % frames_to_skip == 0:
                output_filename = f"frame_{i}.png"
                output_path = os.path.join(args["output_path"], output_filename)
                logger.info(f"Writing to: {output_path}")
                cv2.imwrite(output_path, frame)
        logger.info("Frame Generator finished")


if __name__ == "__main__":
    generator = FrameGenerator()
    logger.info("Starting Frame Generator")
    generator.run()
