import logging
import os
import sys

from detection.yolo_detector import YOLOPersonDetector

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


def validate_and_create_output_folder(output_dir):
    """
    Makes sure that the directory exists, otherwise it creates it.
    :param output_dir: The directory to validate.
    """
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)


def create_detector(detector_cfg_path, detector_weights_path, detector_classes_path,
                    confidence_thrs,
                    nms_thrs):
    """
    Utility function to assemble a YOLO detector.
    :param detector_cfg_path: The path of YOLO's configuration.
    :param detector_weights_path: The path of the pre-trained model's weights.
    :param detector_classes_path: The path of the classes for the model.
    :param confidence_thrs: The confidence threshold for the predictions.
    :param nms_thrs: The confidence threshold for the non max suppression functionality.
    :return:
    """
    logger.info("Assembling Person Detector")
    logger.info(f"Detector config path: {detector_cfg_path}")
    logger.info(f"Detector weights path: {detector_weights_path}")
    logger.info(f"Detector classes path: {detector_classes_path}")
    logger.info(f"Detector confidence argument: {confidence_thrs}")
    logger.info(f"Detector non max suppression argument: {nms_thrs}")
    detector = YOLOPersonDetector(detector_cfg_path, detector_weights_path, detector_classes_path,
                                  conf_threshold=confidence_thrs, nms_threshold=nms_thrs)
    return detector
