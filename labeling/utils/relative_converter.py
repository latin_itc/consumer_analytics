import glob
import os

import pandas as pd

from settings.settings import LABEL_FILE_REGEX


class RelativeConverter:
    """
    Converts relative coordinates into absolute coordinates given the width and height (resolution) of the target image space.
    """

    def __init__(self, label_dir, image_width, image_height, file_regex=LABEL_FILE_REGEX):
        self.w = image_width
        self.h = image_height
        self.label_dir = os.path.join(label_dir, file_regex)

    def read_labels_from_dir(self):
        """
        Reads labels from a given directory, following the YOLO format: one text file per frame where each line
        contains the relative coordinates of a predicted bounding box.
        :return: A generator with the relative coordinates for a bounding box.
        """
        file_paths = glob.glob(self.label_dir)
        for file_path in file_paths:
            with open(file_path) as f:
                filename = os.path.splitext(os.path.basename(file_path))[0]
                frame = filename[filename.find('_') + 1:]
                for line in f:
                    rel_x, rel_y, rel_w, rel_h = [float(x) for x in line.replace("\n", "").split()[1:]]
                    label_dict = {"frame": int(frame), "rel_x": rel_x, "rel_y": rel_y, "rel_w": rel_w, "rel_h": rel_h}
                    yield label_dict

    def relative_to_absolute_bbox(self, rel_x, rel_y, rel_w, rel_h):
        """
        Converts bounding box relative coordinates to an absolute bounding box by using the image's width and height.
        :param rel_x: Relative X coordinate of the bounding box.
        :param rel_y: Relative Y coordinate of the bounding box.
        :param rel_w: Relative width of the bounding box.
        :param rel_h: Relative height of the bounding box.
        :return: The top-left and bottom-right absolute coordinates of the predicted bounding box.
        """
        center_x, center_y = self.w * rel_x, self.h * rel_y
        abs_w, abs_h = int(self.w * rel_w), int(self.h * rel_h)
        x1, y1 = int(center_x - abs_w / 2), int(center_y - abs_h / 2)
        x2, y2 = x1 + abs_w, y1 + abs_h
        return (x1, y1), (x2, y2)

    def yolo_labels_to_bbox(self):
        """
        Transform YOLO-formatted labels in a given directory into a dataframe of absolute coordinates.
        :return: A dataframe of absolute bounding box coordinates for each prediction of YOLO, where the index is the frame.
        """
        abs_labels = []
        for labeled_frame in self.read_labels_from_dir():
            frame, rel_x, rel_y, rel_w, rel_h = labeled_frame["frame"], labeled_frame["rel_x"], labeled_frame["rel_y"], \
                                                labeled_frame["rel_w"], labeled_frame["rel_h"]
            p1, p2 = self.relative_to_absolute_bbox(rel_x, rel_y, rel_w, rel_h)
            x1, y1, x2, y2 = p1[0], p1[1], p2[0], p2[1]
            abs_labels.append({"frame": frame, "x1": x1, "y1": y1, "x2": x2, "y2": y2})
        return pd.DataFrame(abs_labels).set_index("frame")
