import cv2
import logging
import sys

logging.basicConfig(
    stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

logger = logging.getLogger("LabellingLogger")


class VideoStreamer:
    """
    Streams the frames of a given input stream (a video).
    """

    def __init__(self, input_video_path):
        self.video_path = input_video_path
        self.cap = cv2.VideoCapture(self.video_path)
        logger.info("Total Frames: {}".format(int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))))
        logger.info("Video Height: {}".format(int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))
        logger.info("Video Width: {}".format(int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))))
        logger.info("Video Framerate: {}".format(int(self.cap.get(cv2.CAP_PROP_FPS))))

    def stream_video(self):
        """
        While there exists a frame to return, it returns a frame in a generator-like fashion.
        :return: A generator of frames.
        """
        while self.cap.isOpened():
            ret, frame = self.cap.read()
            if frame is not None:
                yield frame
            else:
                break

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        self.end_stream()

    def end_stream(self):
        """
        Finalizes the stream.
        """
        self.cap.release()
        cv2.destroyAllWindows()
