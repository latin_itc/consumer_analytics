# Running the Labeling Programs

## Count Labeler

Counts the amount of discoveries made by YOLO along with the possibility
to count grounds truths as the user.

### Arguments:

* `input`: obligatory argument, the input video.
* `--output`: where to output the labelling results, defaults to a folder within the labeller directory.
* `--frames`: amount of frames to skip before labeling phase, defaults to 60.
* `--nncounter`: if `True`, will use the detector as well to enrich the results, defaults to `True`.
* `--detectorcfg`: path to the configuration file of the detector.
* `--detectorweights`: path to the weights file of the detector.
* `--detectorclasses`: path to <the classes file of the detector.
* `--confidencethrs`: float value that indicates the confidence threshold to assume a positive detection, defaults to 0.5.
* `--nmsthrs`: float value that indicates the non max suppresion confidence threshold of the detector, defaults to 0.5.
* `--countpeople`: If present runs YOLO along with the ground truth counter.

### Example

```
python count_labeler_run.py /Users/user/Downloads/1m-rb.mp4
 --frames 60
 --detectorcfg /Users/user/consumer_analytics/detection/config/yolov3.cfg 
 --detectorweights /Users/user/consumer_analytics/detection/config/yolov3.weights 
 --detectorclasses /Users/user/consumer_analytics/detection/config/yolov3.txt 
 --confidencethrs 0.3 
 --nmsthrs 0.4
 --countpeople
```

## YOLO Labeler

Runs YOLO against a given directory in the parameters, returns the bounding boxes
detected for each image. Returns a DataFrame by default, if `--outdir` is present
then it will write it as a `.csv as well.

### Parameters

* `img_dir`: The directory where the images can be found.
* `--outdir`: Optional directory to output the results.
* `--detectorcfg`: path to the configuration file of the detector.
* `--detectorweights`: path to the weights file of the detector.
* `--detectorclasses`: path to <the classes file of the detector.
* `--confidencethrs`: float value that indicates the confidence threshold to assume a positive detection, defaults to 0.5.
* `--nmsthrs`: float value that indicates the non max suppresion confidence threshold of the detector, defaults to 0.5.

### Example

```python yolo_labeler_run.py 
/Users/user/Developer/df_salon_frames 
--outdir /Users/user/Developer/ITC/consumer_analytics/labeling/output/test.csv 
--detectorcfg /Users/user/Developer/ITC/consumer_analytics/detection/config/yolov3.cfg 
--detectorweights /Users/user/Developer/ITC/consumer_analytics/detection/config/yolov3.weights 
--detectorclasses /Users/user/Developer/ITC/consumer_analytics/detection/config/yolov3.txt 
--confidencethrs 0.4 
--nmsthrs 0.4
```

## IoU Metrics

Writes a `.csv` containing the intersection of union between each detection by
YOLO and the ground truths. The false detections by YOLO lack a ground truth.

### Parameters

* `truth_dir`: The directory containing the `.txt` labels as per YOLO label formatting. There should be one `.txt` file per frame.
* `pred_file`: The `.csv` containing the predictions (bounding boxes) by YOLO.
* `output_file`: The path into which to output the IoU metrics as a `.csv`.
* `image_width`: The width to use with the images to calculate the absolute bounding boxes.
* `image_height`: The height to use with the images to calculate the absolute bounding boxes.
* `--iou`: The **Intersection of Union** threshold to use in order to classify as a **True Positive**.

### Example:

```
python iou_metrics.py 
/Users/juanamari/Developer/labelImg/labels/ 
/Users/juanamari/Developer/ITC/consumer_analytics/labeling/output/test.csv 
/Users/juanamari/Developer/ITC/consumer_analytics/labeling/output/iou_results.csv 
1280 
720 
--iou 0.5
```

## Frame Generator

Creates frames from a given start up to the amount of frames decided and saves them into a directory.

### Parameters

* `input`: The video to generate frames from.
* `output`: Output directory to write the frames to.
* `--frames`: The amount of frames to skip for every image (use 1 to not skip any), defaults to 15.
* `--startframe`: Frame number at which to start generating.
* `--endframe`: Amount of frames to write skipping the frames of the `--frames` parameter.

### Example

```
python frame_generator.py 
/Users/user/Downloads/DeanFunes_ch3_main_20190118090000_20190118100000.asf 
/tmp/df_salon_frames/ 
--frames 15 
--startframe 32400 
--endframe 1350
```